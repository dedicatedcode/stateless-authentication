package com.dedicatedcode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.savedrequest.RequestCache;

@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        AuthenticationSuccessHandler successHandler = getApplicationContext().getBean(AuthenticationSuccessHandler.class);

        JwtCookieRequestCache jwtCookieRequestCache = getApplicationContext().getBean(JwtCookieRequestCache.class);
        JWTSecurityContextRepository securityContextRepository = getApplicationContext().getBean(JWTSecurityContextRepository.class);
        http
                .requestCache().requestCache(jwtCookieRequestCache)
                .and()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .securityContext().securityContextRepository(securityContextRepository)
                .and()
                    .csrf().csrfTokenRepository(new CookieCsrfTokenRepository())
                .and()
                    .authorizeRequests()
                        .antMatchers(HttpMethod.POST, "todo/new").authenticated()
                        .antMatchers(HttpMethod.POST, "todo/delete").authenticated()
                        .antMatchers(HttpMethod.GET, "todo/saved-request.html").authenticated()
                        .antMatchers("/css/**").permitAll()
                        .antMatchers("/js/**").permitAll()
                        .antMatchers("/favicon.ico**").permitAll()
                        .antMatchers(HttpMethod.POST,"/login.html**").permitAll()
                        .anyRequest().authenticated()
                .and()
                    .formLogin().loginPage("/login.html")
                        .successHandler(successHandler)
                            .permitAll()
                .and()
                    .logout().logoutUrl("/logout");
    }

    @Bean
    public UserDetailsService userDetailsService() {
        InMemoryUserDetailsManager inMemoryUserDetailsManager = new InMemoryUserDetailsManager();
        inMemoryUserDetailsManager.createUser(User.withDefaultPasswordEncoder().username("admin").password("admin").roles("ADMIN").build());
        inMemoryUserDetailsManager.createUser(User.withDefaultPasswordEncoder().username("user").password("user").roles("USER").build());
        return inMemoryUserDetailsManager;
    }

    @Bean
    public JWTSecurityContextRepository contextRepository(UserDetailsManager detailsManager,
                                                          @Value("${auth.token}") String tokenName,
                                                          @Value("${auth.secret}") String secret) {
        return new JWTSecurityContextRepository(detailsManager, tokenName, secret);
    }

    @Bean
    public SavedRequestAwareAuthenticationSuccessHandler authenticationSuccessHandler(RequestCache cache) {
        SavedRequestAwareAuthenticationSuccessHandler requestAwareAuthenticationSuccessHandler = new SavedRequestAwareAuthenticationSuccessHandler();
        requestAwareAuthenticationSuccessHandler.setRequestCache(cache);
        return requestAwareAuthenticationSuccessHandler;
    }

    @Bean
    public JwtCookieRequestCache requestCache(@Value("${auth.secret}") String secret) {
        return new JwtCookieRequestCache(secret);
    }

}
